import os
current_path = os.path.dirname(os.path.abspath(__file__))

# Read file
f = open(os.path.join(current_path, '../src/files/version.txt'))

# Grab the Major.Minor.Build version into array
v = f.readline().split('.')
f.close()

# Increment Build number
newVersion = ".".join([v[0], v[1], str(int(v[2]) + 1)])

# Save new version number out
f = open(os.path.join(current_path, '../src/files/version.txt'), 'w')
f.write(newVersion)
f.close()
